CXX      = g++

CXXFLAGS= $(shell root-config --cflags)
LIBS    = $(shell root-config --libs)

SOURCES = ./src/run.cxx ./src/Variables.cxx ./src/Anal_Hit.cxx ./src/ElinkFEBFiberMap.cxx ./src/ElinkMapping.cxx #./src/ABMapping.cxx ./src/StripChannelMap.cxx ./src/WireChannelMap.cxx ./src/PadChannelMap.cxx
HEADERS = ./NSWSTGCMapping/Variables.h ./NSWSTGCMapping/Anal_Hit.h ./NSWSTGCMapping/ElinkFEBFiberMap.h ./NSWSTGCMapping/ElinkMapping.h #./include/ABMapping.h ./include/StripChannelMap.h ./include/WireChannelMap.h ./include/PadChannelMap.h
OBJECTS = $(SOURCES:.cxx=.o)

EXECUTABLE = analHit

all: $(SOURCES) $(EXECUTABLE)

%.o: %.cxx $(HEADERS)
	@echo Compiling $<...
	$(CXX) $(CXXFLAGS) -c -o $@ $< 

$(EXECUTABLE): $(OBJECTS)
	@echo "Linking $(EXECUTABLE) ..."
	@echo "@$(CXX) $(LIBS) $(OBJECTS) -o $@" #-I/opt/wandbox/boost-1.65.1/gcc-head/include -std=gnu++03 -lstdc++fs
	@$(CXX) -o $@ $^ $(LIBS) #-I/opt/wandbox/boost-1.65.1/gcc-head/include -std=gnu++03 "-lstdc++fs"
	@echo "done"

# Specifying the object files as intermediates deletes them automatically after the build process.
.INTERMEDIATE:  $(OBJECTS)

# The default target, which gives instructions, can be called regardless of whether or not files need to be updated.
.PHONY : clean
clean:
	rm -f $(OBJECTS) $(EXECUTABLE)

###
Variables.o: Variables.h
Anal_Hit.o:Variables.h Anal_Hit.h


