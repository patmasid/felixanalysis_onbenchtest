#ifndef ANALYZE_HIT_H
#define ANALYZE_HIT_H

#include <iostream>
#include <fstream>
#include <cmath>
#include "Variables.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TFile.h"
#include "TLine.h"
#include "TLorentzVector.h"
#include <vector>
#include "TGraph.h"
#include <fstream>

#include "ElinkFEBFiberMap.h"
#include "ElinkMapping.h"

//#include <experimental/filesystem>

#include "TString.h"

using namespace std;
//using namespace std::tr2::sys;

//namespace fs = std::experimental::filesystem;

class Analyze_Hit : public Variables{

 public:
  Analyze_Hit(const char *inputFile, const char *outFileName="histo.root", const char *isTestPulse="noTP", const char *isELinkID = "no-ELinkID");
  //Analyze_Hit(const TString &, const char*, const char*); 
  ~Analyze_Hit();
  Bool_t   FillChain(TChain *chain, const string &inputFile);
  Long64_t LoadTree(Long64_t entry);
  void     InitBenchTestValues(const char* boardName_str, const char* middle_num_str, const char* SCA_str);
  void     InitMapHistograms(const char *ElinkMapFile, const char *SorL, const char *PorC); //, const char* VMMidMapFile);
  void CreateHistDir();
  void     EventLoop();
  void     End();
  void     BookHistogram(const char *);
  void     FindProbChannels();
  void     PrintProblematicChannels();
  void     SaveHistograms();
  TFile *oFile;

  //int total_fibers = 4;
  //int total_FEBs_perFiber = 3;
  int total_quads;
  int total_layers;
  int total_types_FEBs;
  int total_vmms_per_sFEB;
  int total_vmms_per_pFEB;
  int total_chans_pervmm;

  int total_boards;
  
  ElinkMapping *m_ElinkMap;

  std::string s_SorL, s_PorC;
  bool isSmall, isPivot;

  bool b_isELinkID;

  vector<int> v_numVMMs;
  vector<int> v_VMMstart;
  vector<std::string> v_FEBType;
  vector<std::string> v_BoardName;
  vector<int> v_SCAid;
  vector<int> v_middlevalue;

  vector<vector<vector<TH1F *>>> h_level1Id;
  vector<vector<vector<TH1F *>>> h_pdo;
  vector<vector<vector<TH1F *>>> h_tdo;
  vector<vector<vector<TH1F *>>> h_relbcid;
  vector<vector<vector<TH2F *>>> h_relbcid_vs_tdo;

  vector<TH1F *> h_Hits_Vs_ChannelNo;
  vector<TH2F *> h_Pdo_Vs_ChannelNo;
  vector<TH2F *> h_Tdo_Vs_ChannelNo;
  vector<vector<TH1F*>> h_Hits_Vs_ChannelNo_perVMM;
  vector<vector<TH2F*>> h_Channel_Vs_level1Id;
  vector<vector<TH2F*>> h_Pdo_Vs_Channel;
  vector<vector<TH2F*>> h_Tdo_Vs_Channel;

  vector<std::string> boardDirName; 
  vector <std::string> level1idDirName, pdoDirName, tdoDirName, relbcidDirName, relbcid_vs_tdoDirName, Hits_Vs_ChannelDirName, Channel_Vs_level1IdDirName, Pdo_Vs_ChannelDirName, Tdo_Vs_ChannelDirName;

  //================= Physical Channels =====================//

  //==================== Finding and printing problematic channels ====================//
  
  bool b_isTestPulse;

  std::ofstream m_strOutput;
  
  vector< vector<std::pair<int,int> >> v_deadChannels_perFEB;
  
  vector< vector<std::pair<std::pair<int,int>,double >>> v_lowEffChannels_perFEB;

};

#endif

#ifdef Analyze_Hit_cxx

Analyze_Hit::Analyze_Hit(const char *inputFile, const char *outFileName, const char *isTestPulse, const char *isELinkID) {

  TChain *tree = new TChain("nsw");

  if( ! FillChain(tree, inputFile) ) {
    std::cerr << "Cannot get the tree " << std::endl;
  } else {
    std::cout << "Initiating the analysis " << std::endl;
  }

  Variables::Init(tree);

  //BookHistogram(outFileName);
  oFile = new TFile(outFileName, "recreate"); 
  
  string isTP = isTestPulse;
  if(isTP == "TP") {b_isTestPulse = true; std::cout << "Test Pulse Data" << std::endl;}
  else if(isTP == "noTP") {b_isTestPulse = false; std::cout << "NOT Test Pulse Data" << std::endl; }
  else {
    std::cerr << "please provide your last argument as 'TP' (test pulse data)  or 'noTP' (not a test pulse data)" << std::endl; 
    return;
  }

  string s_isELinkID = isELinkID;
  if(s_isELinkID == "ELinkID") b_isELinkID = true;
  else if(s_isELinkID == "no-ELinkID") b_isELinkID = false;
  else {
    std::cerr << "please provide your last argument as 'ELinkID' (ELinkID variable is stored in the ntuples)  or 'no-ELinkID' (ELinkID is not stored)" << std::endl;
    return;
  }

  if(b_isTestPulse) { std::cout << "open text file " << std::endl; m_strOutput.open("output_ProblematicChannels.txt"); }

  std::cout<<"End of construction "<<std::endl;

}

Bool_t Analyze_Hit::FillChain(TChain *chain, const string &inputFile) {

  const char *buffer;
  buffer = inputFile.c_str();

  std::cout << "TreeUtilities : FillChain " << std::endl;
  std::cout << "Adding tree from " << buffer << std::endl;
  chain->Add(buffer);
  
  std::cout << "No. of Entries in this tree : " << chain->GetEntries() << std::endl;
  return kTRUE;
}

Long64_t Analyze_Hit::LoadTree(Long64_t entry) {
  // Set the environment to read one entry                                                
  if (!fChain) return -5;
  Long64_t centry = fChain->LoadTree(entry);
  if (centry < 0) return centry;
  if (!fChain->InheritsFrom(TChain::Class()))  return centry;
  TChain *chain = (TChain*)fChain;
  if (chain->GetTreeNumber() != fCurrent) {
    fCurrent = chain->GetTreeNumber();
    //    Notify();
  }
  return centry;
}

Analyze_Hit::~Analyze_Hit() { 

  delete oFile;
  delete m_ElinkMap;
  
  if (!fChain) return;
  delete fChain->GetCurrentFile();

  std::cout<<"Destructor has been called!! "<<std::endl;
 
}

#endif
