#ifndef ElinkFEBFiberMap_H
#define ElinkFEBFiberMap_H

#include <string>

#include <iostream>
#include <sstream>
#include <fstream>

#include <iterator>

#include <vector>
#include <map>
#include <utility>

#include <algorithm>

#include "TString.h"

#include <boost/bimap.hpp>

class ElinkFEBFiberMap{

 public :

  ElinkFEBFiberMap();
  virtual ~ElinkFEBFiberMap(){};
  
  
  /*typedef boost::bimap< int, std::pair<int, double> > bm_type_FeFiber;
  bm_type_FeFiber  ElinkFEBFiberMapping;
  
  typedef boost::bimap< int, std::tuple<int, int, int, int> > bm_type_LayQuad;
  bm_type_LayQuad  ElinkLayerQuadMapping;*/
  
  std::map< int, std::pair<int, double> > ElinkFEBFiberMapping;
  std::map< int, std::tuple<int, int, int, int> > ElinkLayerQuadMapping;
  std::map< int, std::tuple<int, int, int> > FiberLayerQuadMapping;
  std::map< int, int > VMMid_VMMCaptureID_Mapping; //New
  
  std::stringstream m_sx;

  bool verbose;

  void Init();
  void Clear();
  
  void Clear_VmmidMapping();

  void SetVerbose(bool v);
  
  bool SetElinkLayerQuadMap( int quadNum, int layerNum, int ispFEB, int l1ddc,
			  int elink);
  
  bool SetElinkFEBFiberMap( int FiberNum, double FENum, int elink);

  bool SetFiberLayerQuadMap( int layerNum, int ispFEB, int l1ddc,
			     int fiber);
  
  bool SetVmmidCaptureidMap( int VMMid, int VMMCaptureID); //New 
  
  bool ReturnFEBFiberNumber( int &FiberNum, double &FENum, int elink);
  
  bool ReturnLayerQuadNumberElink(int &quadNum, int &layerNum, int &ispFEB, int &l1ddc, int elink);
  
  bool ReturnLayerQuadNumberFiber(int &layerNum, int &ispFEB, int &l1ddc, int fiber);
  
  bool ReturnVmmid(int &VMMid, int VMMCaptureID); //New

  bool ReturnVmmCaptureID(int VMMid, int &VMMCaptureID); //New

  //bool ReturnElinkFEBNumber(int quadNum, int layerNum, int ispFEB, int FiberNum, double &FENum, std::vector<int> &elinks);
  
};
#endif
  
