#ifndef ElinkMapping_H
#define ElinkMapping_H

#include <string>

#include <iostream>
#include <sstream>
#include <fstream>

#include <iterator>

#include <vector>
#include <map>
#include <utility>

#include "TString.h"

#include "ElinkFEBFiberMap.h"

class ElinkMapping {

 private:
  std::string input_ElinkFEBLayerQuad_raw_, input_VMMid_Captureid_raw_;

 public :
  ElinkMapping(const char *input_ElinkFEBLayerQuad_raw); //, const char* input_VMMid_Captureid_raw);

  std::stringstream m_sx;

  std::string v11_input_xu_elinkfebfiber_file_raw, v12_input_xu_elinkfebfiber_file_raw, v21_input_xu_elinkfebfiber_file_raw, v22_input_xu_elinkfebfiber_file_raw;

  bool verbose;

  ElinkFEBFiberMap * Elink_Mapping;

  void LoadXuMapping();

  void Load_VMMid_Captureid_Mapping();

  void SetVerbose(bool v);
  
  bool ReturnFEBFiberNumber(int &FiberNum, double &FENum, int elink);
  bool ReturnLayerQuadNumberElink(int &quadNum, int &layerNum, int &ispFEB, int &l1ddc, int elink);
  bool ReturnLayerQuadNumberFiber(int &layerNum, int &ispFEB, int &l1ddc, int fiber);
  bool ReturnElinkFEBNumber(int quadNum, int layerNum, int ispFEB, int FiberNum, double &FENum, std::vector<int> &elinks);
  bool ReturnVmmid(int &VMMid, int VMMCaptureID); //New
  bool ReturnVmmCaptureID(int VMMid, int &VMMCaptureID); //New

  virtual ~ElinkMapping(){
    delete Elink_Mapping;
  };

};

#endif
