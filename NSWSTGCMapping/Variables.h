//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Sun May 12 03:28:26 2019 by ROOT version 6.14/04
// from TTree nsw/nsw
// found on file: ROx-nsw-bb5-test-26Apr19-193049.data._0001_simple.root
//////////////////////////////////////////////////////////

#ifndef Variables_h
#define Variables_h

#include "TROOT.h"
#include <TChain.h>
#include <TFile.h>
#include <TLorentzVector.h>
#include <TSelector.h>

// Header file for the classes stored in the TTree if any.
#include "TClonesArray.h"
#include "TObject.h"

// Header file for the classes stored in the TTree if any.
#include "vector"

using namespace std;

class  Variables : public TSelector {
 public :

 Variables(TTree * /*tree*/ =0) : fChain(0) { }
  ~Variables() { }
  void    Init(TTree *tree);
  Bool_t  Notify();
  Int_t   GetEntry(Long64_t entry, Int_t getall = 0) { return fChain ? fChain->GetTree()->GetEntry(entry, getall) : 0; }
  double  DeltaPhi(double, double);
  double  DeltaR(double eta1, double phi1, double eta2, double phi2);
  double  HT (std::vector<TLorentzVector>);
  TLorentzVector MHT(std::vector<TLorentzVector>);
  double CosAngle(TLorentzVector Lep, TLorentzVector V);
  TLorentzVector E_Smear(TLorentzVector Par, Double_t perc_sm);
  //void Plot(const char* rootfile1, char *histname);
  void Plot(const char* rootfile1);
  bool IsPathExist(const std::string &s);
  void printTuple(tuple<int,int,int,int> t);

  TTree          *fChain;   //!pointer to the analyzed TTree or TChain
  Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   /*Bool_t          partial_file;
   vector<unsigned int> *guid;
   vector<unsigned int> *run_number;
   vector<unsigned int> lumiblock;
   vector<unsigned int> sourceId;
   vector<unsigned int> linkId;
   vector<unsigned int> linkType;
   vector<unsigned int> fragmentSize;
   vector<unsigned int> level1Id;
   vector<unsigned int> bcid;
   vector<unsigned int> orbit;
   vector<unsigned int> timeset;
   vector<unsigned int> checksum;
   vector<unsigned int> nhits;
   vector<unsigned int> level0Id;
   vector<unsigned int> missing_data_flags;
   vector<unsigned int> to_flag;
   vector<unsigned int> error_flag;
   vector<unsigned int> vmmid;
   vector<unsigned int> channel;
   vector<unsigned int> pdo;
   vector<unsigned int> tdo;
   vector<unsigned int> relbcid;
   vector<unsigned int> nflag;
   vector<unsigned int> parity;*/

  Bool_t          partial_file;
  UInt_t          guid;
  UInt_t          run_number;
  UInt_t          lumiblock;
  UInt_t          ELinkID;
  UInt_t          L1ID;
  vector<unsigned int> *sourceId;
  vector<unsigned int> *linksAlive;
  vector<unsigned int> *linkId;
  vector<unsigned int> *linkType;
  vector<unsigned int> *fragmentSize;
  vector<unsigned int> *level1Id;
  vector<unsigned int> *bcid;
  vector<unsigned int> *orbit;
  vector<unsigned int> *timeset;
  vector<unsigned int> *checksum;
  vector<unsigned int> *nhits;
  vector<unsigned int> *level0Id;
  vector<unsigned int> *missing_data_flags;
  vector<unsigned int> *to_flag;
  vector<unsigned int> *error_flag;
  vector<unsigned int> *vmmid;
  vector<unsigned int> *channel;
  vector<unsigned int> *pdo;
  vector<unsigned int> *tdo;
  vector<unsigned int> *relbcid;
  vector<unsigned int> *nflag;
  vector<unsigned int> *parity;
   

   // List of branches

  /*
   TBranch        *b_partial_file;   //!
   TBranch        *b_guid;   //!
   TBranch        *b_run_number;   //!
   TBranch        *b_lumiblock;   //!
   TBranch        *b_sourceId;   //!
   TBranch        *b_linkId;   //!
   TBranch        *b_linkType;   //!
   TBranch        *b_fragmentSize;   //!
   TBranch        *b_level1Id;   //!
   TBranch        *b_bcid;   //!
   TBranch        *b_orbit;   //!
   TBranch        *b_timeset;   //!
   TBranch        *b_checksum;   //!
   TBranch        *b_nhits;   //!
   TBranch        *b_level0Id;   //!
   TBranch        *b_missing_data_flags;   //!
   TBranch        *b_to_flag;   //!
   TBranch        *b_error_flag;   //!
   TBranch        *b_vmmid;   //!
   TBranch        *b_channel;   //!
   TBranch        *b_pdo;   //!
   TBranch        *b_tdo;   //!
   TBranch        *b_relbcid;   //!
   TBranch        *b_nflag;   //!
   TBranch        *b_parity;   //!
  */
   // List of branches                                                                                                                                                                                      
   TBranch        *b_partial_file;   //!
   TBranch        *b_guid;   //! 
   TBranch        *b_run_number;   //!
   TBranch        *b_lumiblock;   //!
   TBranch        *b_sourceId;   //!   
   TBranch        *b_linksAlive;   //!
   TBranch        *b_linkId;   //!
   TBranch        *b_linkType;   //!
   TBranch        *b_fragmentSize;   //! 
   TBranch        *b_level1Id;   //! 
   TBranch        *b_bcid;   //! 
   TBranch        *b_orbit;   //! 
   TBranch        *b_timeset;   //! 
   TBranch        *b_checksum;   //! 
   TBranch        *b_nhits;   //! 
   TBranch        *b_level0Id;   //! 
   TBranch        *b_missing_data_flags;   //! 
   TBranch        *b_to_flag;   //! 
   TBranch        *b_error_flag;   //!
   TBranch        *b_vmmid;   //!
   TBranch        *b_channel;   //!
   TBranch        *b_pdo;   //!
   TBranch        *b_tdo;   //!
   TBranch        *b_relbcid;   //! 
   TBranch        *b_nflag;   //! 
   TBranch        *b_parity;   //! 

   /*Variables(TTree *tree=0);
   virtual ~Variables();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);*/
};

#endif

#ifdef Variables_cxx
/*Variables::Variables(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("ROx-nsw-bb5-test-26Apr19-193049.data._0001_simple.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("ROx-nsw-bb5-test-26Apr19-193049.data._0001_simple.root");
      }
      f->GetObject("nsw",tree);

   }
   Init(tree);
}

Variables::~Variables()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t Variables::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t Variables::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}
*/
void Variables::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   /*guid = 0;
   run_number = 0;
   lumiblock = 0;
   sourceId = 0;
   linkId = 0;
   linkType = 0;
   fragmentSize = 0;
   level1Id = 0;
   bcid = 0;
   orbit = 0;
   timeset = 0;
   checksum = 0;
   nhits = 0;
   level0Id = 0;
   missing_data_flags = 0;
   to_flag = 0;
   error_flag = 0;
   vmmid = 0;
   channel = 0;
   pdo = 0;
   tdo = 0;
   relbcid = 0;
   nflag = 0;
   parity = 0;*/

   sourceId = 0;
   linksAlive = 0;
   linkId = 0;
   linkType = 0;
   fragmentSize = 0;
   level1Id = 0;
   bcid = 0;
   orbit = 0;
   timeset = 0;
   checksum = 0;
   nhits = 0;
   level0Id = 0;
   missing_data_flags = 0;
   to_flag = 0;
   error_flag = 0;
   vmmid = 0;
   channel = 0;
   pdo = 0;
   tdo = 0;
   relbcid = 0;
   nflag = 0;
   parity = 0;

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   /*fChain->SetBranchAddress("partial_file", &partial_file, &b_partial_file);
   fChain->SetBranchAddress("guid", &guid, &b_guid);
   fChain->SetBranchAddress("run_number", &run_number, &b_run_number);
   fChain->SetBranchAddress("lumiblock", &lumiblock, &b_lumiblock);
   fChain->SetBranchAddress("sourceId", &sourceId, &b_sourceId);
   fChain->SetBranchAddress("linkId", &linkId, &b_linkId);
   fChain->SetBranchAddress("linkType", &linkType, &b_linkType);
   fChain->SetBranchAddress("fragmentSize", &fragmentSize, &b_fragmentSize);
   fChain->SetBranchAddress("level1Id", &level1Id, &b_level1Id);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("orbit", &orbit, &b_orbit);
   fChain->SetBranchAddress("timeset", &timeset, &b_timeset);
   fChain->SetBranchAddress("checksum", &checksum, &b_checksum);
   fChain->SetBranchAddress("nhits", &nhits, &b_nhits);
   fChain->SetBranchAddress("level0Id", &level0Id, &b_level0Id);
   fChain->SetBranchAddress("missing_data_flags", &missing_data_flags, &b_missing_data_flags);
   fChain->SetBranchAddress("to_flag", &to_flag, &b_to_flag);
   fChain->SetBranchAddress("error_flag", &error_flag, &b_error_flag);
   fChain->SetBranchAddress("vmmid", &vmmid, &b_vmmid);
   fChain->SetBranchAddress("channel", &channel, &b_channel);
   fChain->SetBranchAddress("pdo", &pdo, &b_pdo);
   fChain->SetBranchAddress("tdo", &tdo, &b_tdo);
   fChain->SetBranchAddress("relbcid", &relbcid, &b_relbcid);
   fChain->SetBranchAddress("nflag", &nflag, &b_nflag);
   fChain->SetBranchAddress("parity", &parity, &b_parity);*/

   fChain->SetBranchAddress("partial_file", &partial_file, &b_partial_file);
   fChain->SetBranchAddress("guid", &guid, &b_guid);
   fChain->SetBranchAddress("run_number", &run_number, &b_run_number);
   fChain->SetBranchAddress("lumiblock", &lumiblock, &b_lumiblock);
   fChain->SetBranchAddress("sourceId", &sourceId, &b_sourceId);
   fChain->SetBranchAddress("linksAlive", &linksAlive, &b_linksAlive);
   fChain->SetBranchAddress("linkId", &linkId, &b_linkId);
   fChain->SetBranchAddress("linkType", &linkType, &b_linkType);
   fChain->SetBranchAddress("fragmentSize", &fragmentSize, &b_fragmentSize);
   fChain->SetBranchAddress("level1Id", &level1Id, &b_level1Id);
   fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
   fChain->SetBranchAddress("orbit", &orbit, &b_orbit);
   fChain->SetBranchAddress("timeset", &timeset, &b_timeset);
   fChain->SetBranchAddress("checksum", &checksum, &b_checksum);
   fChain->SetBranchAddress("nhits", &nhits, &b_nhits);
   fChain->SetBranchAddress("level0Id", &level0Id, &b_level0Id);
   fChain->SetBranchAddress("missing_data_flags", &missing_data_flags, &b_missing_data_flags);
   fChain->SetBranchAddress("to_flag", &to_flag, &b_to_flag);
   fChain->SetBranchAddress("error_flag", &error_flag, &b_error_flag);
   fChain->SetBranchAddress("vmmid", &vmmid, &b_vmmid);
   fChain->SetBranchAddress("channel", &channel, &b_channel);
   fChain->SetBranchAddress("pdo", &pdo, &b_pdo);
   fChain->SetBranchAddress("tdo", &tdo, &b_tdo);
   fChain->SetBranchAddress("relbcid", &relbcid, &b_relbcid);
   fChain->SetBranchAddress("nflag", &nflag, &b_nflag);
   fChain->SetBranchAddress("parity", &parity, &b_parity);

   Notify();
}

Bool_t Variables::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

/*void Variables::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t Variables::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
   }*/
#endif // #ifdef Variables_cxx
