#include "NSWSTGCMapping/ElinkMapping.h"
#include "NSWSTGCMapping/ElinkFEBFiberMap.h"

using namespace std;

ElinkMapping::ElinkMapping(const char *input_ElinkFEBLayerQuad_raw){

  input_ElinkFEBLayerQuad_raw_ = input_ElinkFEBLayerQuad_raw;
  input_VMMid_Captureid_raw_ = "/afs/cern.ch/user/p/patmasid/public/swROD_analysis/felixdata_minitreeanalysis/data/VMMID_toVMMCaptureID/VMMID_toVMMCaptureID_Map.txt";

  verbose = false;
  
  Elink_Mapping = new ElinkFEBFiberMap();

  SetVerbose( verbose );


}

void ElinkMapping::SetVerbose(bool v) {
  verbose = v;

  Elink_Mapping->SetVerbose( verbose );

}

void ElinkMapping::LoadXuMapping() {
  
  if (verbose) std::cout << "Loading Elink Layer Quad FEB Mapping " << input_ElinkFEBLayerQuad_raw_ << std::endl;
  
  std::string line;

  std::ifstream myfile(input_ElinkFEBLayerQuad_raw_.c_str());
  
  if ( myfile.is_open() ) {
    
    if (verbose) std::cout << "Open Elink Mapping " << input_ElinkFEBLayerQuad_raw_ << std::endl;

    //Elink_Mapping->Clear();
    
    while ( std::getline ( myfile,line ) ) {
      
      if ( line == "" ) return;
      
      std::string data_str =  line;
      
      if (verbose) std::cout << data_str << std::endl;
      
      std::istringstream iss(line);
      
      std::string L1DDC, Layer, IspFEB, Quad, Fiber, Feb, Elink1, Elink2;
      
      int l1ddcNum, layerNum, ispFEBNum, quadNum, fiberNum;
      //std::vector<int> elinks;
      vector<int> elinks;
      double febNum;

      elinks.resize(2);

      if ( !(iss
             >> L1DDC >> l1ddcNum 
	     >> Layer >> layerNum
	     >> IspFEB >> ispFEBNum 
	     >> Quad >> quadNum 
	     >> Fiber >> fiberNum
	     >> Feb >> febNum 
	     >> Elink1 >> elinks[0]
	     >> Elink2 >> elinks[1]) ) {
	std::cout << "Error: Elink Mapping line failed parsing " << data_str << std::endl;
        //continue;

	}
 
      /*iss
	>> L1DDC >> l1ddcNum
	>> Layer >> layerNum
	>> IspFEB >> ispFEBNum
	>> Quad >> quadNum
	>> Fiber >> fiberNum
	>> Feb >> febNum
	>> Elink1 >> elinks[0]
	>> Elink2 >> elinks[1];*/
		      
      std::cout<<" L1DDC " <<l1ddcNum << " Layer " << layerNum << " IspFEB " << ispFEBNum << " Quad " << quadNum << " Fiber " << fiberNum << " Feb " << febNum << " Elink " << elinks[0] << " "<< elinks[1]  << std::endl;
      
      bool okay_1=0, okay_2=0, okay_3=0;
      
      for(unsigned int ii=0; ii<elinks.size(); ii++){
	//cout<<"Elinks in the maps "<<elinks[ii]<<endl;
	okay_1 = Elink_Mapping->SetElinkFEBFiberMap(fiberNum, febNum, elinks[ii]);
	okay_2 = Elink_Mapping->SetElinkLayerQuadMap(quadNum, layerNum, ispFEBNum, l1ddcNum,elinks[ii]);
	//cout<<"okay_3 "<<okay_3<<endl;
      }
      okay_3 = Elink_Mapping->SetFiberLayerQuadMap(layerNum, ispFEBNum, l1ddcNum,fiberNum);
      if ( !okay_1 ) {
	std::cout << "Error:  ElinkFEBFiber Mapping line entry not parsed correctly" << std::endl;
	std::cout << "\t " << line << std::endl;
      }
      else if(!okay_2) {
	std::cout << "Error:  ElinkLayerQuad Mapping line entry not parsed correctly" << std::endl;
	std::cout << "\t " << line << std::endl;
      }
      else if(!okay_3) {
	std::cout << "Error:  FiberLayerQuad Mapping line entry not parsed correctly" << std::endl;
	std::cout << "\t " << line << std::endl;
      }
    }
  }
  else std::cout<< " File not open ElinkMapping "<<std::endl;

}

void ElinkMapping::Load_VMMid_Captureid_Mapping(){

  if (verbose) std::cout << "Loading VMMid - VMM Capture-id Mapping " << input_VMMid_Captureid_raw_ << std::endl;

  std::string line;

  std::ifstream myfile(input_VMMid_Captureid_raw_.c_str());

  if ( myfile.is_open() ) {

    if (verbose) std::cout << "Open VMM id Mapping " << input_VMMid_Captureid_raw_ << std::endl;

    while ( std::getline ( myfile,line ) ) {

      if ( line == "" ) return;

      std::string data_str =  line;

      if (verbose) std::cout << data_str << std::endl;

      std::istringstream iss(line);

      std::string vmmID, vmmCaptureID;
      int vmmIDNum, vmmCaptureIDNum;
      
      iss
        >> vmmID >> vmmIDNum 
        >> vmmCaptureID >> vmmCaptureIDNum;

      std::cout<<"vmm "<<vmmIDNum <<" vmm capture id "<<vmmCaptureIDNum<<std::endl;
      
      bool okay=0;
      
      okay = Elink_Mapping->SetVmmidCaptureidMap(vmmIDNum, vmmCaptureIDNum);
      if ( !okay ) {
	std::cout << "Error: VmmidCaptureid Mapping line entry not parsed correctly " << std::endl;
	std::cout << "\t " << line << std::endl;
      }
    }
  }

}

bool ElinkMapping::ReturnFEBFiberNumber(int &FiberNum, double &FENum, int elink){

  bool foundNum=false;

  double v_FENum = -1.0;
  int v_FiberNum = -1;
  
  foundNum=Elink_Mapping->ReturnFEBFiberNumber(v_FiberNum, v_FENum, elink);

  if(!foundNum){
    if(verbose) cout<< "ReturnFEBFiberNumber: Elink not found"<<endl;
  }
  else{ 
    FiberNum = v_FiberNum;
    FENum = v_FENum;
  }
  //cout<<" Fiber "<<v_FiberNum<<" FEB "<<v_FENum<<" elink "<<elink<<endl;
  return foundNum;

}

bool ElinkMapping::ReturnLayerQuadNumberElink(int &quadNum, int &layerNum, int &ispFEB, int &l1ddc, int elink){

  bool foundNum=false;

  int v_quadNum = -1;
  int v_layerNum = -1;
  int v_ispFEB = -1;
  int v_l1ddc = -1;
  
  foundNum=Elink_Mapping->ReturnLayerQuadNumberElink(v_quadNum, v_layerNum, v_ispFEB, v_l1ddc, elink);

  if(!foundNum){
    if(verbose) cout<< "ReturnLayerQuadNumberElink: Elink not found"<<endl;
  }
  else{
    quadNum = v_quadNum;
    layerNum = v_layerNum;
    ispFEB = v_ispFEB;
    l1ddc = v_l1ddc;
  }
  return foundNum;
}

bool ElinkMapping::ReturnLayerQuadNumberFiber(int &layerNum, int &ispFEB, int &l1ddc, int fiber){

  bool foundNum=false;

  int v_layerNum = -1;
  int v_ispFEB = -1;
  int v_l1ddc = -1;

  foundNum=Elink_Mapping->ReturnLayerQuadNumberFiber(v_layerNum, v_ispFEB, v_l1ddc, fiber);

  if(!foundNum){
    if(verbose) std::cout<< "ReturnLayerQuadNumberFiber: Fiber not found"<<std::endl;
  }
  else{
    layerNum = v_layerNum;
    ispFEB = v_ispFEB;
    l1ddc = v_l1ddc;
  }
  return foundNum;
}

bool ElinkMapping::ReturnVmmid(int &VMMid, int VMMCaptureID){
  
  bool foundNum=false;
  
  int v_VMMid = -1;
  
  foundNum=Elink_Mapping->ReturnVmmid(v_VMMid,VMMCaptureID);
  
  if(!foundNum){
    if(verbose) std::cout<< "ReturnVmmid: VMMCaptureID not found "<<std::endl;
  }
  else{
    VMMid = v_VMMid;
    //std::cout<<"ActualVMMid "<<VMMid<<" Capture VMM id "<<VMMCaptureID<<std::endl;
  }
  return foundNum;

}

bool ElinkMapping::ReturnVmmCaptureID(int VMMid, int &VMMCaptureID){

  bool foundNum=false;

  int v_VMMCaptureID =-1;

  foundNum=Elink_Mapping->ReturnVmmCaptureID(VMMid, v_VMMCaptureID);

  if(!foundNum){
    if(verbose) std::cout<< "ReturnVmmCaptureID: VMMCaptureID not found "<<std::endl;
  }
  else{
    VMMCaptureID = v_VMMCaptureID;
  }
  return foundNum;

}

/*bool ElinkMapping::ReturnElinkFEBNumber(int quadNum, int layerNum, int ispFEB, int FiberNum, double &FENum, std::vector<int> &elinks){

  double v_FENum = -1.0;
  std::vector<int> v_elinks;
  v_elinks.resize(2);
  v_elinks[0]=-1;
  v_elinks[1]=-1;

  Elink_Mapping->ReturnElinkFEBNumber(quadNum, layerNum, ispFEB, FiberNum, v_FENum, v_elinks);

  FENum = v_FENum;
  elinks = v_elinks;

  }*/

