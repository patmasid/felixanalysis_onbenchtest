#include "NSWSTGCMapping/ElinkFEBFiberMap.h"

using namespace std;

ElinkFEBFiberMap::ElinkFEBFiberMap(){
  Init();
  verbose = false;
}

void ElinkFEBFiberMap::SetVerbose(bool v) {
  verbose = v;
}

void ElinkFEBFiberMap::Init(){
  
  int l1ddc = -1;

  for(int iL=1; iL<=4; iL++){
    for(int iispFEB=0; iispFEB<2; iispFEB++){
      
      for(int iQ=1; iQ<=3; iQ++){
	
	if(iL==1 && iispFEB==0) l1ddc=1;
	else if(iL==1 && iispFEB==1) l1ddc=2;
	else if(iL==2 && iispFEB==0) l1ddc=3;
	else if(iL==2 && iispFEB==1) l1ddc=4;
	else if(iL==3 && iispFEB==0) l1ddc=5;
	else if(iL==3 && iispFEB==1) l1ddc=6;
	else if(iL==4 && iispFEB==0) l1ddc=7;
	else if(iL==4 && iispFEB==1) l1ddc=8;
	
	//===== Initiating ElinkLayerQuadMapping ==========//
	ElinkLayerQuadMapping.insert(std::make_pair(-1, std::make_tuple(iQ,iL,iispFEB,l1ddc)));
	ElinkLayerQuadMapping.insert(std::make_pair(-1, std::make_tuple(iQ,iL,iispFEB,l1ddc)));
	//===== Initiating ElinkFEBFiberMapping ==========//

	if(iQ==1){
	  ElinkFEBFiberMapping.insert(std::make_pair(-1, std::make_pair(-1,1.1)));
	  ElinkFEBFiberMapping.insert(std::make_pair(-1, std::make_pair(-1,1.2)));
	}
	else{
	  ElinkFEBFiberMapping.insert(std::make_pair(-1, std::make_pair(-1,iQ)));
          ElinkFEBFiberMapping.insert(std::make_pair(-1, std::make_pair(-1,iQ)));
	}	
      }
      
      //===== Initiating FiberLayerQuadMapping ==========//
      
      FiberLayerQuadMapping.insert(std::make_pair(-1, std::make_tuple(iL,iispFEB,l1ddc)));

    }
  }
  
  //===== Initiating VMMid_VMMCaptureID_Mapping ==========// 
  
  for(int iVMM=0; iVMM<8; iVMM++){
    VMMid_VMMCaptureID_Mapping.insert(std::make_pair(iVMM,-1));
  }

}

void ElinkFEBFiberMap::Clear_VmmidMapping(){

  VMMid_VMMCaptureID_Mapping.clear();

}

void ElinkFEBFiberMap::Clear() {

  ElinkLayerQuadMapping.clear();

  ElinkFEBFiberMapping.clear();
  
  FiberLayerQuadMapping.clear();

  //VMMid_VMMCaptureID_Mapping.clear();

}


bool ElinkFEBFiberMap::SetElinkLayerQuadMap(int quadNum, int layerNum, int ispFEB, int l1ddc,
					   int elink) {

  bool is_okay = true;
  
  //-----------------------------------------------//                                                                                                                                                     

  if ( !( quadNum >= 1 && quadNum <= 3 ) ) {
    is_okay = false;
    cout<<"Quad Num Problem "<<endl;
  }

  if ( !( layerNum >= 1 && layerNum <= 4 ) ) {
    is_okay = false;
    cout<<"Layer Num problem "<<endl;
  }

  if ( !is_okay ) {
    std::cout << "Elink-Layer-Quadruplet Mapping Failed" << std::endl;
    std::cout << " Quad" << quadNum << " Layer " << layerNum << " ispFEB " << ispFEB
              << " Elink "<< elink << std::endl;
  }
  
  else{
    if ( verbose ) {
      std::cout << "Setting Elink Layer Quadruplet mapping: ";
      std::cout << " quad number " << quadNum;
      std::cout << " layer number " << layerNum;
      std::cout << " is pFEB " << ispFEB;
      std::cout << " Elink "<< elink << std::endl;
    } 

    ElinkLayerQuadMapping.insert(std::make_pair(elink, std::make_tuple(quadNum,layerNum,ispFEB,l1ddc)));  
				 
  }
  
  return is_okay;
    
}

bool ElinkFEBFiberMap::SetElinkFEBFiberMap(int FiberNum, double FENum,
                                           int elink) {

  bool is_okay = true;
  
  //if(FENum>3.0 || FENum <1.0) is_okay=false;
  
  if(!is_okay) {
    std::cout << "Elink-FEB-Fiber Mapping Failed" << std::endl;
    std::cout << "Fiber number " << FiberNum 
              << " FE Number " << FENum
              << " Elink "<< elink << std::endl;
  }
  else{
    if(verbose) {
      std::cout << "Setting Elink FEB Fiber mapping: ";
      std::cout << " Fiber number " << FiberNum;
      std::cout << " FE number " << FENum;
      std::cout << " Elink"<< elink << std::endl;
    }

    ElinkFEBFiberMapping.insert(std::make_pair(elink, std::make_pair(FiberNum,FENum)));
    
  }

  return is_okay;

}

bool ElinkFEBFiberMap::SetFiberLayerQuadMap(int layerNum, int ispFEB, int l1ddc,
					    int fiber) {

  bool is_okay = true;

  //-----------------------------------------------//                                                                                                                                                       

  if ( !( layerNum >= 1 && layerNum <= 4 ) ) {
    is_okay = false;
    cout<<"Layer Num problem "<<endl;
  }

  if ( !is_okay ) {
    std::cout << "Fiber-Layer-Quadruplet Mapping Failed" << std::endl;
    std::cout << " Layer " << layerNum << " ispFEB " << ispFEB
              << " Fiber "<< fiber << std::endl;
  }

  else{
    if ( verbose ) {
      std::cout << "Setting Fiber Layer Quadruplet mapping: ";
      std::cout << " layer number " << layerNum;
      std::cout << " is pFEB " << ispFEB;
      std::cout << " Fiber "<< fiber << std::endl;
    }
    
    FiberLayerQuadMapping.insert(std::make_pair(fiber, std::make_tuple(layerNum,ispFEB,l1ddc)));
    tuple<int, int, int> blah = FiberLayerQuadMapping[fiber];
    if(verbose){
      cout<<"tuple 0 "<<get<0>(blah)<<endl;
      cout<<"tuple 1 "<<get<1>(blah)<<endl;
      cout<<"tuple 2 "<<get<2>(blah)<<endl;
    }
  }

  return is_okay;

}

bool ElinkFEBFiberMap::SetVmmidCaptureidMap( int VMMid, int VMMCaptureID){
  
  bool is_okay = true;
  
  if ( !( VMMid >=0 && VMMid <= 7 ) || !( VMMCaptureID >=0 && VMMCaptureID <= 7 ) ) {
    is_okay = false;
    cout<<"VMMid or VMMCaptureID is not within the range "<<endl;
  }
  
  if ( !is_okay ) {
    std::cout << "VMM-id VMM-Capture-id Mapping Failed" <<std::endl;
    std::cout << " VMM-id " << VMMid << " VMM-Capture-id " << VMMCaptureID <<std::endl;
  }
  else{
    if ( verbose ) {
      std::cout << "Setting VMM-id VMM-Capture-id mapping: ";
      std::cout << " VMM-id " << VMMid;
      std::cout << " VMM-Capture-id " << VMMCaptureID;
    }
    
    //VMMid_VMMCaptureID_Mapping.insert(std::make_pair(VMMCaptureID,VMMid));
    VMMid_VMMCaptureID_Mapping[VMMCaptureID]=VMMid;

  }

  return is_okay;

}

bool ElinkFEBFiberMap::ReturnFEBFiberNumber(int &FiberNum, double &FENum, int elink){

  bool foundNum = false;

  if(elink<0){
    cout<<"ReturnFEBFiberNumber(int &FiberNum, double &FENum, int elink): Elink cannot be a negative number "<<endl;
  }

  else{

    std::pair<int, double> fefiber_pair;

    std::map <int, std::pair<int, double> >::iterator X = ElinkFEBFiberMapping.find(elink);

    if(X != ElinkFEBFiberMapping.end() ) { 
      if ( verbose ) cout<<"ReturnFEBFiberNumber : Elink "<<elink<<" found "<<endl;  
      fefiber_pair = X->second;
      FiberNum = fefiber_pair.first;                                                                                                                 
      FENum = fefiber_pair.second;                                                                                                                                                     
      foundNum = true;                                                                                               
    }                                                                                                                         
    else{ if(verbose) cout<<"ReturnFEBFiberNumber: Elink not found "<<endl;}
  }
  
  return ( foundNum );
  
}

bool ElinkFEBFiberMap::ReturnLayerQuadNumberElink(int &quadNum, int &layerNum, int &ispFEB, int &l1ddc, int elink){

  bool foundNum = false;

  if(elink<0){
    cout<<"ReturnLayerQuadNumberElink: Elink cannot be a negative number "<<endl;
  }
  
  else{
    //std::map <std::tuple<int, int, int, int>, std::tuple<int,double,std::vector<int>> >::iterator X;
    std::tuple<int, int, int, int> LayQuadpFEB_tuple;
    
    std::map <int, std::tuple<int, int, int, int> >::iterator X = ElinkLayerQuadMapping.find(elink);
    
    if(X != ElinkLayerQuadMapping.end() ) {
      if ( verbose ) cout<<"ReturnLayerQuadNumberElink : Elink "<<elink<<" found "<<endl;
      LayQuadpFEB_tuple = X->second;
      quadNum = get<0>(LayQuadpFEB_tuple);
      layerNum = get<1>(LayQuadpFEB_tuple);
      ispFEB = get<2>(LayQuadpFEB_tuple);
      l1ddc = get<3>(LayQuadpFEB_tuple);
      foundNum = true;
    }
    else{ if(verbose) cout<<"ReturnLayerQuadNumberElink: Elink not found "<<elink<<endl;}
  }

  return ( foundNum );

}

bool ElinkFEBFiberMap::ReturnLayerQuadNumberFiber(int &layerNum, int &ispFEB, int &l1ddc, int fiber){

  bool foundNum = false;

  if(fiber<0){
    cout<<"ReturnLayerQuadNumberFiber: Fiber cannot be a negative number "<<endl;
  }

  else{
    std::tuple<int, int, int> LayQuadpFEB_tuple;

    std::map <int, std::tuple<int, int, int> >::iterator X = FiberLayerQuadMapping.find(fiber);

    if(X != FiberLayerQuadMapping.end() ) {
      if ( verbose ) std::cout<<"ReturnLayerQuadNumberFiber : Fiber "<<fiber<<" found "<<std::endl;
      LayQuadpFEB_tuple = X->second;
      layerNum = get<0>(LayQuadpFEB_tuple);
      ispFEB = get<1>(LayQuadpFEB_tuple);
      l1ddc = get<2>(LayQuadpFEB_tuple);
      foundNum = true;
    }
    else{ if(verbose) std::cout<<"ReturnLayerQuadNumberFiber: Fiber not found "<<std::endl;}
  }

  return ( foundNum );

}

bool ElinkFEBFiberMap::ReturnVmmid(int &VMMid, int VMMCaptureID){

  bool foundNum = false;
  
  if(VMMCaptureID>7 || VMMCaptureID<0){
    cout<<"ReturnVmmid: vmm capture id should be in the range 0 to 7 "<<endl;
  }
  else{
    int actualVMMid;
    std::map <int,int >::iterator X = VMMid_VMMCaptureID_Mapping.find(VMMCaptureID);
    
    if(X != VMMid_VMMCaptureID_Mapping.end() ) {
      if(verbose) std::cout<<"ReturnVmmid : VMM-Capture-ID " << VMMCaptureID <<" found "<<std::endl;
      actualVMMid = X->second;
      VMMid = actualVMMid;
      foundNum = true;
    }
    else {if(verbose) std::cout<<"ReturnVmmid: VMM-Capture-ID not found "<<std::endl;}
  }
  
  return foundNum;

}

bool ElinkFEBFiberMap::ReturnVmmCaptureID(int VMMid, int &VMMCaptureID){

  bool foundNum = false;

  if(VMMid>7 || VMMid<0){
    cout<<"ReturnVmmid: vmm capture id should be in the range 0 to 7 "<<endl;
  }
  else{
    int VMMidCapture;

    for(std::map<int,int>::iterator iter = VMMid_VMMCaptureID_Mapping.begin(); iter != VMMid_VMMCaptureID_Mapping.end(); ++iter)
      {
	
	if(iter->second == VMMid){
	  VMMidCapture = iter->first;
	  VMMCaptureID=VMMidCapture;
	  foundNum = true;
	}
	
      }
    if(foundNum == false) {if(verbose) std::cout<<"ReturnVmmCaptureID: VMM-ID not found "<<std::endl;}
  }
  return foundNum;

}

/*bool ElinkFEBFiberMap::ReturnElinkFEBNumber(int quadNum, int layerNum, int ispFEB, int FiberNum, double &FENum, std::vector<int> &elinks){
  
  bool foundNum = false;
  
  for ( auto X : ElinkFEBFiberMapping ){
    if(X.first == std::make_tuple(layerNum,ispFEB,FiberNum,quadNum)){
      FENum = get<1>(X.second);
      elinks = get<2>(X.second);
      foundNum = true;
    }
  }
  return ( foundNum );
  }*/
