#define Analyze_Hit_cxx

#include <iostream>
#include <vector>
#include <cstring>
#include "NSWSTGCMapping/Analyze_Hit.h"
#include "NSWSTGCMapping/Variables.h"
#include "NSWSTGCMapping/ElinkFEBFiberMap.h"
#include "NSWSTGCMapping/ElinkMapping.h"
#include <cmath>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <algorithm>

#include <TH1F.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <array>

#include <iomanip>
#include "THStack.h"
#include <math.h>
#include <TLine.h>
#include <TArrow.h>
#include <TLegend.h>

#include "TApplication.h" 
#include <TSystem.h>

#include <fstream>
#include <cstdint>
//#include <filesystem>
#include <sys/stat.h>

#include "TProfile.h"

using namespace std;

void Analyze_Hit::InitBenchTestValues( const char* boardName_str, const char* middle_num_str, const char* SCA_str ){

  total_quads = 3;
  total_layers = 1;
  total_types_FEBs = 2;
  total_vmms_per_sFEB = 8;
  total_vmms_per_pFEB = 3;
  total_chans_pervmm = 64;

  total_boards = 3;

  std::string s_boardName_str = boardName_str;
  std::string s_middle_num_str = middle_num_str;
  std::string s_SCA_str = SCA_str;

  std::vector<std::string> v_temp_BoardName;
  std::vector<int> v_temp_middlevalue;
  std::vector<int> v_temp_SCAid;

  std::string delimiter = ",";
  size_t pos = 0;
  std::string token;
  int i_token= -1;

  //================= Creating temporary board name vector

  while ((pos = s_boardName_str.find(delimiter)) != std::string::npos) {
    token = s_boardName_str.substr(0, pos);
    v_temp_BoardName.push_back(token);
    //std::cout << token << std::endl;
    s_boardName_str.erase(0, pos + delimiter.length());
  }
  v_temp_BoardName.push_back(s_boardName_str);

  //================= Creating temporary middle value vector

  while ((pos = s_middle_num_str.find(delimiter)) != std::string::npos) {
    token = s_middle_num_str.substr(0, pos);
    std::stringstream ss(token);
    if( !(ss >> i_token) ) std::cout << "Middle Value : input strings should have all integers" << std::endl;
    v_temp_middlevalue.push_back(i_token);
    //std::cout << i_token << std::endl;
    s_middle_num_str.erase(0, pos + delimiter.length());
  }
  std::stringstream ss_m( s_middle_num_str);
  if( !(ss_m >> i_token) ) std::cout << "Middle Value : input strings should have all integers" << std::endl;
  v_temp_middlevalue.push_back(i_token);

  //================= Creating temporary SCA id vector 

  while ((pos = s_SCA_str.find(delimiter)) != std::string::npos) {
    token = s_SCA_str.substr(0, pos);
    std::stringstream ss(token);
    if( !(ss >> i_token) ) std::cout << "SCAID : input strings should have all integers" << std::endl;
    v_temp_SCAid.push_back(i_token);
    //std::cout << token << std::endl;
    s_SCA_str.erase(0, pos + delimiter.length());
  }
  std::stringstream ss_s( s_SCA_str);
  if( !(ss_s >> i_token) ) std::cout << "SCAID : input strings should have all integers" << std::endl;
  v_temp_SCAid.push_back(i_token);

  //=================== Only 3 board info allowed ===============//
  
  if(v_temp_BoardName.size()!=total_boards || v_temp_middlevalue.size()!=total_boards || v_temp_SCAid.size()!=total_boards){
    std::cout << "BoardName, MiddleValue, SCAid input strings should have info of " << total_boards << " boards." << std::endl;
    return;
  }

  v_BoardName.resize(total_boards);
  v_SCAid.resize(total_boards);

  v_numVMMs.resize(total_boards);
  v_VMMstart.resize(total_boards);

  //=================== Here assign board names and SCA ids ====================//                                                                                                                          
  for(int iBoard=0; iBoard < total_boards; iBoard++){
    
    v_BoardName[iBoard] = v_temp_BoardName[iBoard];
    v_SCAid[iBoard] = v_temp_SCAid[iBoard];
    if(v_temp_middlevalue[iBoard]==2 || v_temp_middlevalue[iBoard]==3 || v_temp_middlevalue[iBoard]==4) v_numVMMs[iBoard]=3;
    else v_numVMMs[iBoard]=v_temp_middlevalue[iBoard];

    std::cout << "v_BoardName[iBoard]: " << v_BoardName[iBoard] << " v_SCAid[iBoard]: " << v_SCAid[iBoard] << " v_temp_middlevalue[iBoard]: " << v_temp_middlevalue[iBoard] << " v_numVMMs[iBoard]: " << v_numVMMs[iBoard] << std::endl;

  }
  
  //=============================================                                                                                                                                                           


  //===================== Have to be changed ==================//                                                                                                                                           

  for(int iFEB=0; iFEB < total_boards; iFEB++){
    
    if(v_numVMMs[iFEB]==6) v_VMMstart[iFEB]=2;
    else v_VMMstart[iFEB]=0;

  }

  v_FEBType.resize(total_boards);

  for(int iFEB=0; iFEB < total_boards; iFEB++){
    if(v_numVMMs[iFEB]==3) v_FEBType[iFEB]="pFEB";
    else if(v_numVMMs[iFEB]==6 ) v_FEBType[iFEB]="sFEB6";
    else if(v_numVMMs[iFEB]==8 ) v_FEBType[iFEB]="sFEB8";
    else v_FEBType[iFEB]="No-FEB";
  }
 

  std::cout << "End of InitBenchTestValues" << std::endl;

}

void Analyze_Hit::InitMapHistograms(const char *ElinkMapFile, const char *SorL, const char *PorC){ //, const char* VMMidMapFile){
  
  //========= Info about the wedge ==================//
  
  s_SorL = SorL;
  s_PorC = PorC;
  
  if(s_SorL=="S") isSmall=true;
  else if(s_SorL=="L") isSmall=false;
  else std::cerr<<"Please provide S or L as third argument"<<std::endl;

  std::cout << SorL << " " << s_SorL << " " << isSmall << std::endl;

  if(s_PorC=="P") isPivot=true;
  else if(s_PorC=="C") isPivot=false;
  else std::cerr<<"Please provide P or C as fourth argument"<<std::endl;

  std::cout << PorC << " " << s_PorC << " " << isPivot << std::endl;

  //================ Booking the histograms ====================//                                                                       

  m_ElinkMap = new ElinkMapping(ElinkMapFile); //,VMMidMapFile);
  m_ElinkMap->SetVerbose(false);
  m_ElinkMap->LoadXuMapping();
  m_ElinkMap->Load_VMMid_Captureid_Mapping();

  //================ Booking the histograms ====================// 

  h_level1Id.resize(total_boards);
  h_pdo.resize(total_boards);
  h_tdo.resize(total_boards);
  h_relbcid.resize(total_boards);
  h_relbcid_vs_tdo.resize(total_boards);
  h_Hits_Vs_ChannelNo.resize(total_boards);
  h_Hits_Vs_ChannelNo_perVMM.resize(total_boards);
  h_Pdo_Vs_ChannelNo.resize(total_boards);
  h_Tdo_Vs_ChannelNo.resize(total_boards);
  h_Channel_Vs_level1Id.resize(total_boards);
  h_Pdo_Vs_Channel.resize(total_boards);
  h_Tdo_Vs_Channel.resize(total_boards);
  
  boardDirName.resize(total_boards);
  level1idDirName.resize(total_boards);
  pdoDirName.resize(total_boards);
  tdoDirName.resize(total_boards);
  relbcidDirName.resize(total_boards);
  relbcid_vs_tdoDirName.resize(total_boards);
  Hits_Vs_ChannelDirName.resize(total_boards);
  Channel_Vs_level1IdDirName.resize(total_boards);
  Pdo_Vs_ChannelDirName.resize(total_boards);
  Tdo_Vs_ChannelDirName.resize(total_boards);

  v_deadChannels_perFEB.resize(total_boards);
  
  v_lowEffChannels_perFEB.resize(total_boards);
  
  for(int iBoard=0; iBoard<total_boards; iBoard++){
    
    if( v_numVMMs[iBoard]==0 ) continue;

    h_level1Id[iBoard].resize(v_numVMMs[iBoard]);
    h_pdo[iBoard].resize(v_numVMMs[iBoard]);
    h_tdo[iBoard].resize(v_numVMMs[iBoard]);
    h_relbcid[iBoard].resize(v_numVMMs[iBoard]);
    h_relbcid_vs_tdo[iBoard].resize(v_numVMMs[iBoard]);
    h_Hits_Vs_ChannelNo_perVMM[iBoard].resize(v_numVMMs[iBoard]);
    h_Channel_Vs_level1Id[iBoard].resize(v_numVMMs[iBoard]);
    h_Pdo_Vs_Channel[iBoard].resize(v_numVMMs[iBoard]);
    h_Tdo_Vs_Channel[iBoard].resize(v_numVMMs[iBoard]);
   
    for(int iVMM=v_VMMstart[iBoard]; iVMM < v_VMMstart[iBoard]+v_numVMMs[iBoard]; iVMM++){
      h_level1Id[iBoard][iVMM].resize(total_chans_pervmm);
      h_pdo[iBoard][iVMM].resize(total_chans_pervmm);
      h_tdo[iBoard][iVMM].resize(total_chans_pervmm);
      h_relbcid[iBoard][iVMM].resize(total_chans_pervmm);
      h_relbcid_vs_tdo[iBoard][iVMM].resize(total_chans_pervmm);
    }
  }
  
  
  for(int iBoard=0; iBoard<total_boards; iBoard++){

    if( v_numVMMs[iBoard]==0 ) continue;
    
    string name_level1Id, name_pdo, name_tdo, name_relbcid, name_relbcid_vs_tdo, name_Hits_Vs_chanNo, name_Channel_Vs_level1Id, name_Pdo_Vs_Channel, name_Tdo_Vs_Channel, FEBType, name_NoConnOhysChan_Hits_Vs_chanNo, name_Hits_Vs_Channel_perVMM, name_Pdo_Vs_chanNo, name_Tdo_Vs_chanNo;
    //int numVMMs=-1;
    
    name_Hits_Vs_chanNo="Hits_Vs_TotalChanNo_SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard];
    h_Hits_Vs_ChannelNo[iBoard]=new TH1F(name_Hits_Vs_chanNo.c_str(),name_Hits_Vs_chanNo.c_str(),v_numVMMs[iBoard]*64,-0.5,v_numVMMs[iBoard]*64-0.5);
    h_Hits_Vs_ChannelNo[iBoard]->SetDirectory(0);
    
    name_Pdo_Vs_chanNo="Pdo_Vs_TotalChanNo_SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard];
    h_Pdo_Vs_ChannelNo[iBoard]=new TH2F(name_Pdo_Vs_chanNo.c_str(),name_Pdo_Vs_chanNo.c_str(),v_numVMMs[iBoard]*64,-0.5,v_numVMMs[iBoard]*64-0.5,1024,-0.5,1023.5);
    h_Pdo_Vs_ChannelNo[iBoard]->SetDirectory(0);
    
    name_Tdo_Vs_chanNo="Tdo_Vs_TotalChanNo_SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard];
    h_Tdo_Vs_ChannelNo[iBoard]=new TH2F(name_Tdo_Vs_chanNo.c_str(),name_Tdo_Vs_chanNo.c_str(),v_numVMMs[iBoard]*64,-0.5,v_numVMMs[iBoard]*64-0.5,256,-0.5,255.5);
    h_Tdo_Vs_ChannelNo[iBoard]->SetDirectory(0);

    for(int iVMM=v_VMMstart[iBoard]; iVMM < v_VMMstart[iBoard]+v_numVMMs[iBoard]; iVMM++){
      
      name_Hits_Vs_Channel_perVMM = "Hits_Vs_TotalChanNo_SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard]+"_vmmid"+std::to_string(iVMM);
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]=new TH1F(name_Hits_Vs_Channel_perVMM.c_str(),name_Hits_Vs_Channel_perVMM.c_str(),64,-0.5,63.5);
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->SetDirectory(0);
      
      name_Channel_Vs_level1Id="Channel_Vs_level1Id_SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard]+"_vmmid"+std::to_string(iVMM);
      h_Channel_Vs_level1Id[iBoard][iVMM]=new TH2F(name_Channel_Vs_level1Id.c_str(),name_Channel_Vs_level1Id.c_str(),66000,-0.5,65999.5,64,-0.5,63.5);
      h_Channel_Vs_level1Id[iBoard][iVMM]->SetDirectory(0);
      name_Pdo_Vs_Channel="Pdo_Vs_Channel_SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard]+"_vmmid"+std::to_string(iVMM);
      h_Pdo_Vs_Channel[iBoard][iVMM]=new TH2F(name_Pdo_Vs_Channel.c_str(),name_Pdo_Vs_Channel.c_str(),64,-0.5,63.5,1024,-0.5,1023.5);
      h_Pdo_Vs_Channel[iBoard][iVMM]->SetDirectory(0);
      name_Tdo_Vs_Channel="Tdo_Vs_Channel_SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard]+"_vmmid"+std::to_string(iVMM);
      h_Tdo_Vs_Channel[iBoard][iVMM]=new TH2F(name_Tdo_Vs_Channel.c_str(),name_Tdo_Vs_Channel.c_str(),64,-0.5,63.5,256,-0.5,255.5);
      h_Tdo_Vs_Channel[iBoard][iVMM]->SetDirectory(0);
      
      //======== Determine range of connected channels ==========//
      /*
	std::vector< std::pair<int, int> > ElecChanPairVec;
	
	bool ispFEB = false;
	if(iispFEB == 0) ispFEB = false;
	else if(iispFEB == 1) ispFEB = true;
	
	std::cout << "Quad: " << iQ+1 << " Layer: " << iL+1 << " ispFEB: " << ispFEB << " vmm: " << iVMM << std::endl;
	
	ElecChanPairVec = m_ABMap->ReturnRange_ConnectedElecChan(iVMM, ispFEB,
	isSmall, isPivot,
	iQ+1, iL+1);
      */
      for(int iChan=0; iChan<total_chans_pervmm; iChan++){
	
	name_level1Id="level1Id_SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard]+"_vmmid"+std::to_string(iVMM)+"_chan"+std::to_string(iChan);
	name_pdo="pdo_SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard]+"_vmmid"+std::to_string(iVMM)+"_chan"+std::to_string(iChan);
	name_tdo="tdo_SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard]+"_vmmid"+std::to_string(iVMM)+"_chan"+std::to_string(iChan);
	name_relbcid="relbcid_SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard]+"_vmmid"+std::to_string(iVMM)+"_chan"+std::to_string(iChan);
	name_relbcid_vs_tdo="relbcid_vs_tdo_SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard]+"_vmmid"+std::to_string(iVMM)+"_chan"+std::to_string(iChan);
	
	h_level1Id[iBoard][iVMM][iChan]=new TH1F(name_level1Id.c_str(),name_level1Id.c_str(),66000,-0.5,65999.5);
	h_level1Id[iBoard][iVMM][iChan]->SetDirectory(0);
	h_pdo[iBoard][iVMM][iChan]=new TH1F(name_pdo.c_str(),name_pdo.c_str(),1024,-0.5,1023.5);
	h_pdo[iBoard][iVMM][iChan]->SetDirectory(0);
	h_tdo[iBoard][iVMM][iChan]=new TH1F(name_tdo.c_str(),name_tdo.c_str(),256,-0.5,255.5);
	h_tdo[iBoard][iVMM][iChan]->SetDirectory(0);
	h_relbcid[iBoard][iVMM][iChan]=new TH1F(name_relbcid.c_str(),name_relbcid.c_str(),8,-0.5,7.5);
	h_relbcid[iBoard][iVMM][iChan]->SetDirectory(0);
	h_relbcid_vs_tdo[iBoard][iVMM][iChan]=new TH2F(name_relbcid_vs_tdo.c_str(),name_relbcid_vs_tdo.c_str(),256,-0.5,255.5,8,-0.5,7.5);
	h_relbcid_vs_tdo[iBoard][iVMM][iChan]->SetDirectory(0);
	
      }
    }
  }
}

void Analyze_Hit::CreateHistDir(){

  for(int iBoard=0; iBoard<total_boards; iBoard++){
    
    boardDirName[iBoard] = "SCA_"+std::to_string(v_SCAid[iBoard])+"_"+v_FEBType[iBoard]+"/";
    oFile->mkdir(boardDirName[iBoard].c_str());
    
    Hits_Vs_ChannelDirName[iBoard] = boardDirName[iBoard]+"Hits_Vs_Channel/";
    oFile->mkdir(Hits_Vs_ChannelDirName[iBoard].c_str());
    
    Channel_Vs_level1IdDirName[iBoard] = boardDirName[iBoard]+"Channel_Vs_level1Id/";
    oFile->mkdir(Channel_Vs_level1IdDirName[iBoard].c_str());
    
    Pdo_Vs_ChannelDirName[iBoard] = boardDirName[iBoard]+"Pdo_Vs_Channel/";
    oFile->mkdir(Pdo_Vs_ChannelDirName[iBoard].c_str());
    
    Tdo_Vs_ChannelDirName[iBoard] = boardDirName[iBoard]+"Tdo_Vs_Channel/";
    oFile->mkdir(Tdo_Vs_ChannelDirName[iBoard].c_str());
    
    level1idDirName[iBoard] = boardDirName[iBoard]+"level1Id/";
    oFile->mkdir(level1idDirName[iBoard].c_str());
    
    pdoDirName[iBoard] = boardDirName[iBoard]+"pdo/";
    oFile->mkdir(pdoDirName[iBoard].c_str());
    
    tdoDirName[iBoard] = boardDirName[iBoard]+"tdo/";
    oFile->mkdir(tdoDirName[iBoard].c_str());
    
    relbcidDirName[iBoard] = boardDirName[iBoard]+"relbcid/";
    oFile->mkdir(relbcidDirName[iBoard].c_str());
    
    relbcid_vs_tdoDirName[iBoard] = boardDirName[iBoard]+"relbcid/";
    oFile->mkdir(relbcid_vs_tdoDirName[iBoard].c_str());
    
  }
}

void Analyze_Hit::EventLoop() {

  if (fChain == 0) {std::cout << "empty tree" << std::endl; return;}
  
  //std::cout << "Dataset " << data << std::endl;
  
  Long64_t nentries = fChain->GetEntriesFast();
  cout << "nentries " << nentries << endl;
  
  Long64_t nbytes = 0, nb = 0;
  int decade = 0;
  
  //========================== Defining variables ============================//

  int event_number=0;
  
  //========================== Variables for TGraph ============================//
  
  vector<unsigned int> vecX_HitsVsPdoTdo, vecY_HitsVsPdoTdo, vecZ_HitsVsPdoTdo, vecX_PdoTdoVsChan, vecY_PdoTdoVsChan, vecZ_PdoTdoVsChan;

  // loop over entries of the tree                                                                                                                                                                          
  for (Long64_t jentry=0; jentry<nentries; jentry++) {
    
    //=== print fraction of total events processed ====                                                                                                                                                     
    double progress = 10.0 * jentry / (1.0 * nentries);
    int k = int (progress);
    if (k > decade) cout << 10 * k << " %" << endl;
    decade = k;
    
    //===== read this entry =====                                                                                                                                                                           
    Long64_t ientry = LoadTree(jentry);
    if (ientry < 0) break;
    
    nb = fChain->GetEntry(jentry);
    nbytes += nb;
    // if (Cut(ientry) < 0) continue;                                                                                                                                                                      
    
    fChain->SetBranchStatus("*",0); //disable all branches
    fChain->SetBranchStatus("run_number",1);
    fChain->SetBranchStatus("linkId",1);
    fChain->SetBranchStatus("level1Id",1);
    fChain->SetBranchStatus("vmmid",1);
    fChain->SetBranchStatus("channel",1);
    fChain->SetBranchStatus("pdo",1);
    fChain->SetBranchStatus("tdo",1);
    fChain->SetBranchStatus("relbcid",1);
    
    
    event_number+=1;

    std::cout << "Event: " << event_number << std::endl;
    
    //============== Variables New =================//
    
    int Total_No_Hits;
    Total_No_Hits=vmmid->size();
    cout<<"Total Hits: "<<Total_No_Hits<<endl;
    
    //unsigned int arrX[Total_No_Hits*]

    int VmmRealId=-1;

    //===================== Filling the histograms =====================//

    //std::cout << "Total_No_Hits: " << Total_No_Hits << " vmmid_size: " << vmmid->size() << std::endl;
    
    for(int ii=0; ii<Total_No_Hits; ii++){

      int quadNum, layerNum, ispFEB, l1ddcNum, FEBnum, Fibernum;
      double FEB;
      //std::cout<<"link id "<<linkId->at(ii)<<" quad "<<quadNum<<" layer "<<layerNum<<" ispFEB "<<ispFEB<<std::endl;
      //bool isElinkPresent = m_ElinkMap->ReturnLayerQuadNumberElink(quadNum, layerNum, ispFEB, l1ddcNum, linkId->at(ii));
      
      bool isElinkPresent = m_ElinkMap->ReturnFEBFiberNumber(Fibernum, FEB, linkId->at(ii));

      if(!isElinkPresent) continue;
      
      FEBnum = FEB;

      //std::cout << "Link ID: " << linkId->at(ii) << std::endl;

      m_ElinkMap->ReturnVmmid(VmmRealId, vmmid->at(ii));

      //std::cout<<"vmm roc id "<<vmmid->at(ii)<<" vmm real id "<<VmmRealId<<" channel "<<channel->at(ii)<<std::endl;

      h_level1Id[FEBnum-1][VmmRealId][channel->at(ii)]->Fill(level1Id->at(ii));
      h_pdo[FEBnum-1][VmmRealId][channel->at(ii)]->Fill(pdo->at(ii));
      h_tdo[FEBnum-1][VmmRealId][channel->at(ii)]->Fill(tdo->at(ii));
      h_relbcid[FEBnum-1][VmmRealId][channel->at(ii)]->Fill(relbcid->at(ii));
      h_relbcid_vs_tdo[FEBnum-1][VmmRealId][channel->at(ii)]->Fill(tdo->at(ii),relbcid->at(ii));
      h_Hits_Vs_ChannelNo[FEBnum-1]->Fill( 64*VmmRealId+channel->at(ii) );
      h_Hits_Vs_ChannelNo_perVMM[FEBnum-1][VmmRealId]->Fill( channel->at(ii) );
      h_Pdo_Vs_ChannelNo[FEBnum-1]->Fill( 64*VmmRealId+channel->at(ii),pdo->at(ii) );
      h_Tdo_Vs_ChannelNo[FEBnum-1]->Fill( 64*VmmRealId+channel->at(ii),tdo->at(ii) );
      h_Channel_Vs_level1Id[FEBnum-1][VmmRealId]->Fill( level1Id->at(ii),channel->at(ii) );
      h_Pdo_Vs_Channel[FEBnum-1][VmmRealId]->Fill( channel->at(ii),pdo->at(ii) );
      h_Tdo_Vs_Channel[FEBnum-1][VmmRealId]->Fill( channel->at(ii),tdo->at(ii) );
      
    }
    
  }
  
}

void Analyze_Hit::FindProbChannels(){
  
  if(b_isTestPulse){
    
    for(int iBoard=0; iBoard<total_boards; iBoard++){
      
      int total_channels_FEB = h_Hits_Vs_ChannelNo[iBoard]->GetNbinsX();
      int max_hist_FEB = h_Hits_Vs_ChannelNo[iBoard]->GetMaximum();
      for(int iBin=1; iBin <= total_channels_FEB; iBin++){
	
	if(h_Hits_Vs_ChannelNo[iBoard]->GetBinContent(iBin) == 0){
	  
	  std::pair<int, int> currPair = std::make_pair((iBin-1)/64, (iBin-1)%64);
	  v_deadChannels_perFEB[iBoard].push_back(currPair);
	  
	}
	
	else if(h_Hits_Vs_ChannelNo[iBoard]->GetBinContent(iBin) < max_hist_FEB){
	  
	  std::pair<int, int> currPair = std::make_pair((iBin-1)/64, (iBin-1)%64);
	  std::pair< std::pair<int,int>,double > currElecChanPair = std::make_pair(currPair,h_Hits_Vs_ChannelNo[iBoard]->GetBinContent(iBin)/max_hist_FEB);
	  v_lowEffChannels_perFEB[iBoard].push_back(currElecChanPair);
	  
	  
	}
	
      }
      
      
      
    }
    
  }
  
}

void Analyze_Hit::PrintProblematicChannels(){

  if(b_isTestPulse){
    
    m_strOutput << "SUMMARY OF PROBLEMATIC CHANNELS: Dead and Inefficient Channels " << std::endl;
    
    m_strOutput << std::endl;
    
    for(int iBoard=0; iBoard < total_boards; iBoard++){
      
      m_strOutput << "BoardName" << " " << "SCAID" << " " << "num_DeadChannels" << " " << "num_LowEffChannels" << " " << "DeadChannels" << " " << "LowEffChannels" << std::endl;
      
      m_strOutput << v_BoardName[iBoard] << " " << v_SCAid[iBoard] << " " << v_deadChannels_perFEB[iBoard].size() << " " << v_lowEffChannels_perFEB[iBoard].size() << " ";
      
      if(v_deadChannels_perFEB[iBoard].size() == 0) m_strOutput << "-------- ";
      
      else{
	for(unsigned int iChan=0; iChan < v_deadChannels_perFEB[iBoard].size(); iChan++){
	  
	  m_strOutput << v_deadChannels_perFEB[iBoard][iChan].first << "(" << v_deadChannels_perFEB[iBoard][iChan].second << "),";
	  
	}
      }
      
      m_strOutput << " " ;
      
      if(v_lowEffChannels_perFEB[iBoard].size() == 0) m_strOutput << "-------- ";
      
      else{
	for(unsigned int iChan=0; iChan < v_lowEffChannels_perFEB[iBoard].size(); iChan++){
	  
	  m_strOutput << v_lowEffChannels_perFEB[iBoard][iChan].first.first << "(" << v_lowEffChannels_perFEB[iBoard][iChan].first.second << "(" << v_lowEffChannels_perFEB[iBoard][iChan].second << "),";
	  
	}
      }
      
      m_strOutput << std::endl;
      
    }
    
  }
  
}


void Analyze_Hit::SaveHistograms(){

  string outputDir, hTit, hXTit, hYTit;
  //Double_t Xmin, Xmax, Ymin, Ymax;
  
  for(int iBoard=0; iBoard<total_boards; iBoard++){
    
    hTit=h_Hits_Vs_ChannelNo[iBoard]->GetName();
    hXTit="Total Channel No.";
    h_Hits_Vs_ChannelNo[iBoard]->GetXaxis()->SetTitle(hXTit.c_str());
    h_Hits_Vs_ChannelNo[iBoard]->GetXaxis()->SetTitleSize(0.05);
    h_Hits_Vs_ChannelNo[iBoard]->GetXaxis()->SetTitleOffset(0.8);
    h_Hits_Vs_ChannelNo[iBoard]->SetTitle(hTit.c_str());
    h_Hits_Vs_ChannelNo[iBoard]->SetLineColor(kBlack);
    h_Hits_Vs_ChannelNo[iBoard]->SetLineWidth(3);
    h_Hits_Vs_ChannelNo[iBoard]->GetXaxis()->SetNdivisions(8);
    h_Hits_Vs_ChannelNo[iBoard]->SetStats(0);
    h_Hits_Vs_ChannelNo[iBoard]->SetMinimum(0);
    
    hTit=h_Pdo_Vs_ChannelNo[iBoard]->GetName();
    hXTit="Total Channel No.";
    hYTit="Pdo";
    h_Pdo_Vs_ChannelNo[iBoard]->GetXaxis()->SetTitle(hXTit.c_str());
    h_Pdo_Vs_ChannelNo[iBoard]->GetXaxis()->SetTitleSize(0.05);
    h_Pdo_Vs_ChannelNo[iBoard]->GetXaxis()->SetTitleOffset(0.8);
    h_Pdo_Vs_ChannelNo[iBoard]->SetTitle(hTit.c_str());
    h_Pdo_Vs_ChannelNo[iBoard]->SetLineColor(kBlack);
    h_Pdo_Vs_ChannelNo[iBoard]->SetLineWidth(3);
    h_Pdo_Vs_ChannelNo[iBoard]->GetXaxis()->SetNdivisions(8);
    h_Pdo_Vs_ChannelNo[iBoard]->SetStats(0);
    h_Pdo_Vs_ChannelNo[iBoard]->SetMinimum(0);
    
    hTit=h_Tdo_Vs_ChannelNo[iBoard]->GetName();
    hXTit="Total Channel No.";
    hYTit="Tdo";
    h_Tdo_Vs_ChannelNo[iBoard]->GetXaxis()->SetTitle(hXTit.c_str());
    h_Tdo_Vs_ChannelNo[iBoard]->GetXaxis()->SetTitleSize(0.05);
    h_Tdo_Vs_ChannelNo[iBoard]->GetXaxis()->SetTitleOffset(0.8);
    h_Tdo_Vs_ChannelNo[iBoard]->SetTitle(hTit.c_str());
    h_Tdo_Vs_ChannelNo[iBoard]->SetLineColor(kBlack);
    h_Tdo_Vs_ChannelNo[iBoard]->SetLineWidth(3);
    h_Tdo_Vs_ChannelNo[iBoard]->GetXaxis()->SetNdivisions(8);
    h_Tdo_Vs_ChannelNo[iBoard]->SetStats(0);
    h_Tdo_Vs_ChannelNo[iBoard]->SetMinimum(0);

    for(int iVMM=v_VMMstart[iBoard]; iVMM < v_VMMstart[iBoard]+v_numVMMs[iBoard]; iVMM++){
      
      hTit=h_Channel_Vs_level1Id[iBoard][iVMM]->GetName();
      hXTit="Level1Id";
      hYTit="Channel No.";
      h_Channel_Vs_level1Id[iBoard][iVMM]->GetXaxis()->SetTitle(hXTit.c_str());
      h_Channel_Vs_level1Id[iBoard][iVMM]->GetYaxis()->SetTitle(hYTit.c_str());
      h_Channel_Vs_level1Id[iBoard][iVMM]->GetXaxis()->SetTitleSize(0.05);
      h_Channel_Vs_level1Id[iBoard][iVMM]->GetXaxis()->SetTitleOffset(0.8);
      h_Channel_Vs_level1Id[iBoard][iVMM]->SetTitle(hTit.c_str());
      h_Channel_Vs_level1Id[iBoard][iVMM]->SetLineColor(kBlack);
      h_Channel_Vs_level1Id[iBoard][iVMM]->SetLineWidth(3);
      h_Channel_Vs_level1Id[iBoard][iVMM]->GetXaxis()->SetNdivisions(8);
      h_Channel_Vs_level1Id[iBoard][iVMM]->SetStats(0);
      
      hTit=h_Pdo_Vs_Channel[iBoard][iVMM]->GetName();
      hXTit="Channel No.";
      hYTit="Pdo";
      h_Pdo_Vs_Channel[iBoard][iVMM]->GetXaxis()->SetTitle(hXTit.c_str());
      h_Pdo_Vs_Channel[iBoard][iVMM]->GetYaxis()->SetTitle(hYTit.c_str());
      h_Pdo_Vs_Channel[iBoard][iVMM]->GetXaxis()->SetTitleSize(0.05);
      h_Pdo_Vs_Channel[iBoard][iVMM]->GetXaxis()->SetTitleOffset(0.8);
      h_Pdo_Vs_Channel[iBoard][iVMM]->SetTitle(hTit.c_str());
      h_Pdo_Vs_Channel[iBoard][iVMM]->SetLineColor(kBlack);
      h_Pdo_Vs_Channel[iBoard][iVMM]->SetLineWidth(3);
      h_Pdo_Vs_Channel[iBoard][iVMM]->GetXaxis()->SetNdivisions(8);
      h_Pdo_Vs_Channel[iBoard][iVMM]->SetStats(0);
      
      hTit=h_Tdo_Vs_Channel[iBoard][iVMM]->GetName();
      hXTit="Channel No.";
      hYTit="Tdo";
      h_Tdo_Vs_Channel[iBoard][iVMM]->GetXaxis()->SetTitle(hXTit.c_str());
      h_Tdo_Vs_Channel[iBoard][iVMM]->GetYaxis()->SetTitle(hYTit.c_str());
      h_Tdo_Vs_Channel[iBoard][iVMM]->GetXaxis()->SetTitleSize(0.05);
      h_Tdo_Vs_Channel[iBoard][iVMM]->GetXaxis()->SetTitleOffset(0.8);
      h_Tdo_Vs_Channel[iBoard][iVMM]->SetTitle(hTit.c_str());
      h_Tdo_Vs_Channel[iBoard][iVMM]->SetLineColor(kBlack);
      h_Tdo_Vs_Channel[iBoard][iVMM]->SetLineWidth(3);
      h_Tdo_Vs_Channel[iBoard][iVMM]->GetXaxis()->SetNdivisions(8);
      h_Tdo_Vs_Channel[iBoard][iVMM]->SetStats(0);
      
      hTit=h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->GetName();
      hXTit="Channel No.";
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->GetXaxis()->SetTitle(hXTit.c_str());
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->GetXaxis()->SetTitleSize(0.05);
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->GetXaxis()->SetTitleOffset(0.8);
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->SetTitle(hTit.c_str());
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->SetLineColor(kBlack);
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->SetLineWidth(3);
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->GetXaxis()->SetNdivisions(8);
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->SetStats(0);
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->SetMinimum(0);
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->SetMaximum(h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->GetMaximum()*1.5);

      for(int iChan=0; iChan<64; iChan++){
	
	//=============== for level1Id histogram ==============//
	hTit=h_level1Id[iBoard][iVMM][iChan]->GetName();
	hXTit="Level1Id";
	h_level1Id[iBoard][iVMM][iChan]->GetXaxis()->SetTitle(hXTit.c_str());
	h_level1Id[iBoard][iVMM][iChan]->GetXaxis()->SetTitleSize(0.05);
	h_level1Id[iBoard][iVMM][iChan]->GetXaxis()->SetTitleOffset(0.8);
	h_level1Id[iBoard][iVMM][iChan]->SetTitle(hTit.c_str());
	h_level1Id[iBoard][iVMM][iChan]->SetLineColor(kBlack);
	h_level1Id[iBoard][iVMM][iChan]->SetLineWidth(3);
	h_level1Id[iBoard][iVMM][iChan]->SetMinimum(0);
	
	//=============== for pdo histogram ==============//
	
	//cout<<" Quad "<<iQ+1<<" Layer "<<iL+1<<" ispFEB "<<iispFEB<<" VMM "<<iVMM<<" Channel "<<iChan<<endl;
	hTit=h_pdo[iBoard][iVMM][iChan]->GetName();
	hXTit="Pdo";
	h_pdo[iBoard][iVMM][iChan]->GetXaxis()->SetTitle(hXTit.c_str());
	h_pdo[iBoard][iVMM][iChan]->GetXaxis()->SetTitleSize(0.05);
	h_pdo[iBoard][iVMM][iChan]->GetXaxis()->SetTitleOffset(0.8);
	h_pdo[iBoard][iVMM][iChan]->SetTitle(hTit.c_str());
	h_pdo[iBoard][iVMM][iChan]->SetLineColor(kBlack);
	h_pdo[iBoard][iVMM][iChan]->SetLineWidth(3);
	h_pdo[iBoard][iVMM][iChan]->SetMinimum(0);
	
	//=============== for tdo histogram ==============//                                                                                           
	
	hTit=h_tdo[iBoard][iVMM][iChan]->GetName();
	hXTit="Tdo";
	h_tdo[iBoard][iVMM][iChan]->GetXaxis()->SetTitle(hXTit.c_str());
	h_tdo[iBoard][iVMM][iChan]->GetXaxis()->SetTitleSize(0.05);
	h_tdo[iBoard][iVMM][iChan]->GetXaxis()->SetTitleOffset(0.8);
	h_tdo[iBoard][iVMM][iChan]->SetTitle(hTit.c_str());
	h_tdo[iBoard][iVMM][iChan]->SetLineColor(kBlack);
	h_tdo[iBoard][iVMM][iChan]->SetLineWidth(3);
	h_tdo[iBoard][iVMM][iChan]->SetMinimum(0);
	
	//=============== for relbcid histogram ==============//
	
	hTit=h_relbcid[iBoard][iVMM][iChan]->GetName();
	hXTit="Relbcid";
	h_relbcid[iBoard][iVMM][iChan]->GetXaxis()->SetTitle(hXTit.c_str());
	h_relbcid[iBoard][iVMM][iChan]->GetXaxis()->SetTitleSize(0.05);
	h_relbcid[iBoard][iVMM][iChan]->GetXaxis()->SetTitleOffset(0.8);
	h_relbcid[iBoard][iVMM][iChan]->SetTitle(hTit.c_str());
	h_relbcid[iBoard][iVMM][iChan]->SetLineColor(kBlack);
	h_relbcid[iBoard][iVMM][iChan]->SetLineWidth(3);
	h_relbcid[iBoard][iVMM][iChan]->SetMinimum(0);
	
	//=============== for relbcid vs tdo histogram ==============//
	
	hTit=h_relbcid_vs_tdo[iBoard][iVMM][iChan]->GetName();
	hXTit="Tdo";
	hYTit="Relbcid";
	h_relbcid_vs_tdo[iBoard][iVMM][iChan]->GetXaxis()->SetTitle(hXTit.c_str());
	h_relbcid_vs_tdo[iBoard][iVMM][iChan]->GetYaxis()->SetTitle(hYTit.c_str());
	h_relbcid_vs_tdo[iBoard][iVMM][iChan]->GetXaxis()->SetTitleSize(0.05);
	h_relbcid_vs_tdo[iBoard][iVMM][iChan]->GetXaxis()->SetTitleOffset(0.8);
	h_relbcid_vs_tdo[iBoard][iVMM][iChan]->SetTitle(hTit.c_str());
	h_relbcid_vs_tdo[iBoard][iVMM][iChan]->SetLineColor(kBlack);
	h_relbcid_vs_tdo[iBoard][iVMM][iChan]->SetLineWidth(3);
	
      }
    }
  }
}

void Analyze_Hit::End() {

  for(int iBoard=0; iBoard<total_boards; iBoard++){
    
    oFile->cd(Hits_Vs_ChannelDirName[iBoard].c_str());
    h_Hits_Vs_ChannelNo[iBoard]->Write("",TObject::kOverwrite);
    delete h_Hits_Vs_ChannelNo[iBoard];
    
    oFile->cd(Pdo_Vs_ChannelDirName[iBoard].c_str());
    h_Pdo_Vs_ChannelNo[iBoard]->Write("",TObject::kOverwrite);
    delete h_Pdo_Vs_ChannelNo[iBoard];
    
    oFile->cd(Tdo_Vs_ChannelDirName[iBoard].c_str());
    h_Tdo_Vs_ChannelNo[iBoard]->Write("",TObject::kOverwrite);
    delete h_Tdo_Vs_ChannelNo[iBoard];

    for(int iVMM=v_VMMstart[iBoard]; iVMM < v_VMMstart[iBoard]+v_numVMMs[iBoard]; iVMM++){
      
      oFile->cd(Hits_Vs_ChannelDirName[iBoard].c_str());
      h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM]->Write("",TObject::kOverwrite);
      delete h_Hits_Vs_ChannelNo_perVMM[iBoard][iVMM];
      oFile->cd(Channel_Vs_level1IdDirName[iBoard].c_str());
      h_Channel_Vs_level1Id[iBoard][iVMM]->Write("",TObject::kOverwrite);
      delete h_Channel_Vs_level1Id[iBoard][iVMM];
      oFile->cd(Pdo_Vs_ChannelDirName[iBoard].c_str());
      h_Pdo_Vs_Channel[iBoard][iVMM]->Write("",TObject::kOverwrite);
      delete h_Pdo_Vs_Channel[iBoard][iVMM];
      oFile->cd(Tdo_Vs_ChannelDirName[iBoard].c_str());
      h_Tdo_Vs_Channel[iBoard][iVMM]->Write("",TObject::kOverwrite);
      delete h_Tdo_Vs_Channel[iBoard][iVMM];
      
      for(int iChan=0; iChan<64; iChan++){
	
	oFile->cd(level1idDirName[iBoard].c_str());
	h_level1Id[iBoard][iVMM][iChan]->Write("",TObject::kOverwrite);
	oFile->cd(pdoDirName[iBoard].c_str());
	h_pdo[iBoard][iVMM][iChan]->Write("",TObject::kOverwrite);
	oFile->cd(tdoDirName[iBoard].c_str());
	h_tdo[iBoard][iVMM][iChan]->Write("",TObject::kOverwrite);
	oFile->cd(relbcidDirName[iBoard].c_str());
	h_relbcid[iBoard][iVMM][iChan]->Write("",TObject::kOverwrite);
	oFile->cd(relbcid_vs_tdoDirName[iBoard].c_str());
	h_relbcid_vs_tdo[iBoard][iVMM][iChan]->Write("",TObject::kOverwrite);
	
	delete h_level1Id[iBoard][iVMM][iChan];
	delete h_pdo[iBoard][iVMM][iChan];
	delete h_tdo[iBoard][iVMM][iChan];
	delete h_relbcid[iBoard][iVMM][iChan];
	delete h_relbcid_vs_tdo[iBoard][iVMM][iChan];
	
      }
    }
  }
  
  oFile->Close();

  std::cout<<" Func End() has ended!"<<std::endl;

}
