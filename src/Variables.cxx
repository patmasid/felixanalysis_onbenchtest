#define Variables_cxx

#include "NSWSTGCMapping/Variables.h"
#include <TH2.h>
#include <TStyle.h>
#include <math.h>

//#include "recipeAUX/OxbridgeMT2/interface/Basic_Mt2_332_Calculator.h"
//#include "recipeAUX/OxbridgeMT2/interface/ChengHanBisect_Mt2_332_Calculator.h"

//#include "SusyAnaTools/TopTagger/interface/Type3TopTagger.h"

#include "TFile.h"
#include "TTree.h"
#include "TChain.h"
//#include "Math/VectorUtil.h"
#include "TRandom.h"

#include <iostream>
#include <cstdio>
#include <string>
#include <ctime>
#include <vector>
#include <map>
#include <set>
#include <string>
#include <TH1F.h>
#include <TStyle.h>
#include <TCanvas.h>
#include "TApplication.h"

#include <fstream>
#include <cstdint>
#include <sys/stat.h>

#include <tuple>

using namespace std;

double Variables::DeltaPhi(double phi1, double phi2) {
  double result = phi1 - phi2;
  while (result > M_PI)    result -= 2 * M_PI;
  while (result <= -M_PI)  result += 2 * M_PI;
  return result;
}

double Variables::DeltaR(double eta1, double phi1, double eta2, double phi2) {
  double deta = eta1 - eta2;
  double dphi = DeltaPhi(phi1, phi2);
  return std::sqrt(deta*deta + dphi*dphi);
}

double Variables::HT (std::vector<TLorentzVector> vjets) {
  double ht = 0.0;
  for( unsigned int ijet=0; ijet<vjets.size(); ijet++) {    
    if( vjets[ijet].Pt()>50.0 && std::abs(vjets[ijet].Eta())<2.5 ) 
      ht += vjets[ijet].Pt();
  }
  return ht;
}

TLorentzVector Variables::MHT(std::vector<TLorentzVector> vjets) {;
  TLorentzVector mht(0.0, 0.0, 0.0, 0.0);
  for( unsigned int ijet=0; ijet<vjets.size(); ijet++) {    
    if( vjets[ijet].Pt()>30.0 && std::abs(vjets[ijet].Eta())<5.0 ) 
      mht -= vjets[ijet];
  }

  return mht;
}

double Variables::CosAngle(TLorentzVector Lep, TLorentzVector V) {
  TLorentzVector Lep_VFrame = Lep;
  Double_t Angle_LepVrest_V, CosAngle_LepVrest_V;
  Lep_VFrame.Boost((-1)*V.BoostVector());
  Angle_LepVrest_V=(Lep_VFrame.Vect()).Angle(V.Vect());
  CosAngle_LepVrest_V=cos(Angle_LepVrest_V);
  return CosAngle_LepVrest_V;
}

TLorentzVector Variables::E_Smear(TLorentzVector Par, Double_t perc_sm){
  TLorentzVector Par_ESm;
  Par_ESm=Par;
  Par_ESm.SetE(Par.E()*(1+(perc_sm*gRandom->Gaus(0,1)/200)));
  Double_t Ratio_E_NewToOld=Par_ESm.E()/Par.E();
  //============ Assuming higher energy regime where masses of leptons do not matter ==========//
  Par_ESm.SetPx(Par.Px()*Ratio_E_NewToOld);
  Par_ESm.SetPy(Par.Py()*Ratio_E_NewToOld);
  Par_ESm.SetPz(Par.Pz()*Ratio_E_NewToOld);
  return Par_ESm;    
}

void Variables::Plot(const char* rootfile1){

  gStyle->SetOptStat(0);
  gStyle->SetOptTitle(1);

  TFile *_file1 = TFile::Open(rootfile1);
  
  char hname[500], hXtit[500], hYtit[500], hTit[500];
  Double_t Xmin, Xmax, Ymin, Ymax;
  int no_bins;
  
  sprintf(hXtit, "%s", "Level1Id->at(0)");
  //sprintf(hYtit, "Number of events");                                                                                                                                                                     
  sprintf(hTit, "Distribution of Level1Id");
  no_bins=100;
  Xmin=0;
  Xmax=10000;
  Ymin=0;
  Ymax=100000;
  
  _file1->cd();

  TObject *obj;
  TKey *key;
  TIter next( _file1->GetListOfKeys());
  while ((key = (TKey *) next())) {
    obj = _file1->Get(key->GetName()); // copy object to memory
    const char *histname=key->GetName();
    //printf(" found object:%s\n",key->GetName());
    TH1F *h1 = (TH1F*)_file1->Get(key->GetName());
    //cout<<"wohhhooooo"<<endl;
    //TH1F *h1 = (TH1F*)gDirectory->GetList()->FindObject("myHist");
    //h1->SetName("VMMid0_Link144");
    h1->GetXaxis()->SetTitle(hXtit);
    h1->GetYaxis()->SetTitle(hYtit);
    h1->GetYaxis()->SetTitleSize(0.05);
    h1->GetYaxis()->SetTitleOffset(0.45);
    h1->GetXaxis()->SetTitleSize(0.05);
    h1->GetXaxis()->SetTitleOffset(0.8);
    h1->SetTitle(hTit);
    h1->SetLineColor(kBlack);
    h1->SetLineWidth(3);
    
    char cname[500];
    sprintf(cname, "c_%s", histname);  //histname
    TCanvas *c = new TCanvas(cname, cname,800, 700);
    
    TH1F *vframe=c->DrawFrame(Xmin,Ymin,Xmax,Ymax);
    vframe->GetXaxis()->SetTitle(hXtit);
    h1->Draw();
    
    c->Update();
    c->Draw();
  } 

  cout<<"BLAHHHHHHHHH"<<endl;
  
}

bool IsPathExist(const std::string s)
{
  struct stat buffer;
  return (stat (s.c_str(), &buffer) == 0);
}


