#define run_cxx

#include <iostream>
#include <vector>
#include <cstring>
#include "NSWSTGCMapping/Analyze_Hit.h"
#include "NSWSTGCMapping/Variables.h"
//#include "NSWSTGCMapping/ElinkFEBFiberMap.h"
#include <cmath>
#include <TLorentzVector.h>
#include <TVector3.h>
#include <algorithm>
//#include <TH2.h>                                                                                              
#include <TH1F.h>
#include <TStyle.h>
#include <TCanvas.h>
#include <array>

#include <iomanip>
#include "THStack.h"
#include <math.h>

#include "TApplication.h"
#include <TSystem.h>



using namespace std;

int main(int argc, char* argv[])
{

  if (argc < 8) {
    cerr << "Please give 7 arguments: " << "the minitree to decode (.root file), " << " Elink Mapping txt file, " << " outputFileName, " << " SmallOrLarge, " << " PivotOrConfirm, " << " isTestPulse ('TP' or 'noTP')" << " 'ELinkID' or 'no-ELinkID', " << " BoardName string, " << " middle value string, " << " SCAID string " << endl;
    return -1;
  }
  const char *inputFile          = argv[1];
  const char *ElinkMapInputFile  = argv[2];
  const char *outFileName        = argv[3];
  const char *SmallOrLarge       = argv[4];
  const char *PivotOrConfirm     = argv[5];
  const char *isTestPulse        = argv[6];
  const char *isELinkID          = argv[7];
  const char *boardName          = argv[8];
  const char *middle_num         = argv[9];
  const char *SCA                = argv[10];

  Analyze_Hit ana(inputFile, outFileName, isTestPulse, isELinkID);

  ana.InitBenchTestValues(boardName, middle_num, SCA);
  ana.InitMapHistograms(ElinkMapInputFile, SmallOrLarge, PivotOrConfirm); //,VMMidMapInputFile);
  ana.CreateHistDir();
  ana.EventLoop();
  ana.FindProbChannels();
  ana.PrintProblematicChannels();
  ana.SaveHistograms();
  ana.End();

  return 0;
}
