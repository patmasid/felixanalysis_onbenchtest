# NSWSTGCMapping

* Create a work directory and create following ```CMakeLists.txt```
$ cmake_config

```bash
mkdir netio_felix_analysis
cd netio_felix_analysis
git clone < url >
source ./felixanalysis_onBenchTest/setupEnv.sh
printf "cmake_minimum_required(VERSION 3.4.3)\nfind_package(TDAQ)\ninclude(CTest)\ntdaq_project(NSWDAQ 1.0.0 USES tdaq 8.2.0)\n" > CMakeLists.txt
```

* Build the package

cmake_config   # Create build configuration
cd $CMTCONFIG  # Go to the folder such as x86_64-centos7-gcc8-opt/
make -j        # Build all the programs and libraries

# Some function illustration 
(include directory has all the header files
src directory has all the .cxx files and also the mapping files)

./data/ElinkFebMapping/ has the mapping from elink to fiber, layer, quadruplet, boards, etc.

# How to execute: 

$ cd felixanalysis_onBenchTest

$ source setupEnv.sh


Master Branch:
(It creates an executable called analyzeHit)


$ felixanalysis_onBenchTest/x86_64-centos7-gcc8-opt/felixanalysis_onBenchTest/analyzeHit < path_to_the_input_minitree >, <path_to_the_text_file_with_mapping_of_elinks_to_fiber_boards_layer_quadruplet>, <path_and_name_of_the_output_root-file, e.g. ./output.root>, <'S' or 'L'>, <'P' or 'C'>, <'TP' or 'noTP' (if it is a test pulse data or not)>, <'ELinkID' or 'no-ELinkID' (if ELinkID branch is saved or not)>, <board name string (batchno-middlevalue-scaid) separated by commas>, <middlevalues separated by commas>, <scaids separated by commas>